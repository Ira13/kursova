import {Component, OnInit} from '@angular/core';
import {Edge, Node} from '@swimlane/ngx-graph';
import {createDefaultGraph, Graph} from './graph';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  nodes: Node[] = [];
  edges: Edge[] = [];
  graph: Graph;
  origin: string;
  destination: string;

  paths: {
    nodes: Node[];
    edges: Edge[];
  }[];

  results;

  ngOnInit(): void {
    this.graph = createDefaultGraph();
    for (const [key, value] of Object.entries(this.graph.vertices)) {
      this.nodes.push({
        id: key,
        label: value.data,
      });

      for (const [vertex, weight] of Object.entries(value.adjancencyList)) {
        if (this.edges.find(el => el.id === (key + vertex).replace(/\s/g,''))) {
          return;
        }
        this.edges.push({
          id: (key + vertex).replace(/\s/g,''),
          label: weight as string,
          source: key,
          target: vertex,
        });
      }
    }
  }

  onSearchClick(): void {
    if (this.origin && this.destination) {
      this.results = this.graph.getAllPaths(this.origin, this.destination);
      console.log(this.results);
    }
  }
}
