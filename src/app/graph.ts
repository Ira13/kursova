/**
 * Визначити на карті Києва 15 видатних місць (не розташованих поруч),
 сполучення між ними (вулиці, проспекти) та прокласти всі можливі маршрути,
 що повністю відмінні один від одного.
 *
 * */

export class Vertex {
  adjancencyList = {};
  key;
  data;
  currCost = 0;

  constructor(key, data) {
    this.key = key;
    this.data = data;
  }

  connect(otherVertex, weight) {
    this.adjancencyList[otherVertex] = weight;
  }


  getConnections() {
    return Object.keys(this.adjancencyList);
  }

  getCost(vertex) {
    return this.adjancencyList[vertex];
  }
}


/** This class is a weighted directed graph that is
 supposed to be able to find all paths between two nodes

 * The graph sorts all the paths by weight
 * The graphs vertices uses keys to allow duplicates of data
 * The graphs depth first search is based on recursion
 **/

export class Graph {
  numberOfVertices: number;
  vertices: { [key: string]: Vertex };

  /** graph used to find all paths between two nodes using DFS */
  constructor() {
    this.numberOfVertices = 0;
    this.vertices = {};
  }

  /** adds a vertex to graph and saves vertex based on unique key */
  add(key, data) {
    if (!this.vertices[key]) {
      this.numberOfVertices++;
      this.vertices[key] = new Vertex(key, data);
      return true;
    }
    return false;
  }

  /** Connect two vertexes */
  addEdge(fromVertex, toVertex, weight) {
    if (this.vertices[fromVertex] && this.vertices[toVertex]) {
      this.vertices[fromVertex].connect(toVertex, weight);
      return true;
    }
    return false;
  }

  getAllPaths(start, end) {
    return this.dfs(start, end, [], [], [], [], start);
  }

  /** Знаходить всі унікальні шляхи між двома вершинами та підраховує сумарну вагу */
  dfs(currVertex, destVertex, visited, path, fullPath, unique, start) {
    /** Беремо вершину, так як вона тепер відвідана додаємо її в шлях */
    const vertex = this.vertices[currVertex];
    visited.push(currVertex);
    path.push(vertex.data);
    /** Зберігаємо поточний шлях, якщо досягли точки призначення */
    if (currVertex === destVertex) {
      if (!path.find(item => item !== destVertex && item !== start && unique.includes(item))) {
        unique.push(...path);
        fullPath.push({path: [...path], cost: vertex.currCost});
      }
    }

    /** Рекурсивно викликаємо пошук для кожної сусідньої вершини поточної вершини */
    vertex.getConnections().forEach(item => {
      if (!visited.includes(item)) {
        this.vertices[item].currCost = vertex.getCost(item) + vertex.currCost;
        this.dfs(item, destVertex, visited, path, fullPath, unique, start);
      }
    });

    visited.pop();
    path.pop();


    return fullPath;
  }д
}

export function createDefaultGraph(): Graph {
  const graph = new Graph();
  graph.add('Поштова площа', 'Поштова площа');
  graph.add('Майдан Незалежності', 'Майдан Незалежності');
  graph.add('Пейзажна алея', 'Пейзажна алея');
  graph.add('Музей Історії Києва', 'Музей Історії Києва');
  graph.add('Костел Св. Миколая', 'Костел Св. Миколая');
  graph.add('Батьківщина-Мати', 'Батьківщина-Мати');
  graph.add('Києво-Печерська Лавра', 'Києво-Печерська Лавра');
  graph.add('НСК Олімпійський', 'НСК Олімпійський');
  graph.add('Золоті Ворота', 'Золоті Ворота');
  graph.add('Дім з химерами', 'Дім з химерами');
  graph.add('Палац Спорту', 'Палац Спорту');
  graph.add('Маріїнський палац', 'Маріїнський палац');
  graph.add('Парковий міст', 'Парковий міст');
  graph.add('Аскольдова Могила', 'Аскольдова Могила');


  graph.addEdge('Поштова площа', 'Майдан Незалежності', 2.7);
  graph.addEdge('Майдан Незалежності', 'Поштова площа', 2.7);

  graph.addEdge('Пейзажна алея', 'Майдан Незалежності', 2.2);
  graph.addEdge('Майдан Незалежності', 'Пейзажна Алея', 2.2);

  graph.addEdge('Пейзажна алея', 'Золоті Ворота', 1.5);
  graph.addEdge('Пейзажна алея', 'Золоті Ворота', 1.5);

  graph.addEdge('Пейзажна алея', 'Поштова площа', 3.5);

  graph.addEdge('Музей Історії Києва', 'Золоті Ворота', 0.8);
  graph.addEdge('Золоті Ворота', 'Музей Історії Києва', 0.8);

  graph.addEdge('Майдан Незалежності', 'Золоті Ворота', 1.9);
  graph.addEdge('Золоті Ворота', 'Майдан Незалежності', 1.9);

  graph.addEdge('Майдан Незалежності', 'Парковий міст', 1.9);
  graph.addEdge('Парковий міст', 'Майдан Незалежності', 1.9);

  graph.addEdge('Майдан Незалежності', 'Музей Історії Києва', 1);
  graph.addEdge('Музей Історії Києва', 'Майдан Незалежності', 1);

  graph.addEdge('Майдан Незалежності', 'Маріїнський палац', 1.5);
  graph.addEdge('Маріїнський палац', 'Майдан Незалежності', 1.5);

  graph.addEdge('НСК Олімпійський', 'Музей Історії Києва', 2);
  graph.addEdge('Музей Історії Києва', 'НСК Олімпійський', 2);

  graph.addEdge('Золоті Ворота', 'НСК Олімпійський', 2.4);
  graph.addEdge('НСК Олімпійський', 'Золоті Ворота', 2.4);

  graph.addEdge('Костел Св. Миколая', 'НСК Олімпійський', 0.7);
  graph.addEdge('НСК Олімпійський', 'Костел Св. Миколая', 0.7);

  graph.addEdge('Костел Св. Миколая', 'Батьківщина-Мати', 4.8);
  graph.addEdge('Батьківщина-Мати', 'Костел Св. Миколая', 4.8);

  graph.addEdge('Батьківщина-Мати', 'Києво-Печерська Лавра', 1.7);
  graph.addEdge('Києво-Печерська Лавра', 'Батьківщина-Мати', 1.7);

  graph.addEdge('Палац Спорту', 'Києво-Печерська Лавра', 4.3);
  graph.addEdge('Києво-Печерська Лавра', 'Палац Спорту', 4.3);

  graph.addEdge('Поштова площа', 'Києво-Печерська Лавра', 7.1);
  graph.addEdge('Києво-Печерська Лавра', 'Поштова площа', 7.1);

  graph.addEdge('Аскольдова Могила', 'Києво-Печерська Лавра', 1.6);
  graph.addEdge('Києво-Печерська Лавра', 'Аскольдова Могила', 1.6);

  graph.addEdge('Аскольдова Могила', 'Парковий міст', 1.6);
  graph.addEdge('Парковий міст', 'Аскольдова Могила', 1.6);

  graph.addEdge('Поштова площа', 'Парковий міст', 1.6);
  graph.addEdge('Парковий міст', 'Поштова площа', 1.6);

  graph.addEdge('Поштова площа', 'Пейзажна алея', 1.6);
  graph.addEdge('Пейзажна алея', 'Поштова площа', 1.6);

  graph.addEdge('Києво-Печерська Лавра', 'Маріїнський палац', 2.8);
  graph.addEdge('Маріїнський палац', 'Києво-Печерська Лавра', 2.8);

  graph.addEdge('Маріїнський палац', 'Дім з химерами', 1.1);
  graph.addEdge('Дім з химерами', 'Маріїнський палац', 1.1);

  graph.addEdge('Палац Спорту', 'Дім з химерами', 2.6);
  graph.addEdge('Дім з химерами', 'Палац Спорту', 2.6);

  graph.addEdge('Музей Історії Києва', 'Дім з химерами', 1.3);
  graph.addEdge('Дім з химерами', 'Музей Історії Києва', 1.3);

  graph.addEdge('Палац Спорту', 'НСК Олімпійський', 1.2);
  graph.addEdge('НСК Олімпійський', 'Палац Спорту', 1.2);

  graph.addEdge('Палац Спорту', 'Музей Історії Києва', 2.6);
  graph.addEdge('Музей Історії Києва', 'Палац Спорту', 2.6);

  graph.addEdge('Золоті Ворота', 'Музей Історії Києва', 2.6);
  graph.addEdge('Музей Історії Києва', 'Золоті Ворота', 2.6);

  return graph;
}













